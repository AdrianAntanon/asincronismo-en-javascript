const fetchData = require('../utils/fetchData')

const API = 'https://rickandmortyapi.com/api/character/'

/* for(let i=0; i<10; i++){
    fetchData(API)
        .then(data =>{
            console.log(data.info.count)
            return fetchData(`${API}${data.results[i].id}`)
        })
        .then(data =>{
            console.log(data.name)
            return fetchData(data.origin.url)
        })
        .then(data =>{
            console.log(data.dimension)
        })
        .catch( err => console.error(err))
} */
// Si lo meto en un for da error, ya que realiza el primer then 10 veces y así con el resto, 
// no todos los personajes tienen la misma dimensión y más cosas así, debo hacer el curso de Postman para ver en más profundidad cómo funciona
fetchData(API)
    .then(data =>{
        console.log(data.info.count)
        return fetchData(`${API}${data.results[0].id}`)
    })
    .then(data =>{
        console.log(data.name)
        return fetchData(data.origin.url)
    })
    .then(data =>{
        console.log(data.dimension)
    })
    .catch( err => console.error(err))